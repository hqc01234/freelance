import { TreeItem } from '../shared/models';

export let MAIN_SIDE_BAR_TREE_ITEMS: TreeItem[] = [
    new TreeItem({
        title       : "Products Management",
        icon        : "fa fa-link",
        routerLink  : "",
        childItems  : [
            new TreeItem({
                title       : "Products",
                icon        : "fa fa-link",
                routerLink  : "products/product-list" 
            }),
            new TreeItem({
                title       : "Product Categories",
                icon        : "fa fa-link", 
                routerLink  : "products/product-categories"
            }),
            new TreeItem({
                title       : "Product Details",
                icon        : "fa fa-link", 
                routerLink  : "products/product-details/0",
                isHidden    : true
            })
        ]
    })
]