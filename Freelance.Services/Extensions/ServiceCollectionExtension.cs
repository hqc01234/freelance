﻿using Freelance.Core.DbInitializer;
using Freelance.Core.Identity;
using Freelance.Core.Services;
using Freelance.Data.DBContext;
using Freelance.Data.DbInitializer;
using Freelance.Data.Extensions;
using Freelance.Data.Identity;
using Freelance.Services.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Freelance.Services.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddServicesModule(this IServiceCollection services, IConfiguration configuration)
        {
            //Data module
            services.AddDataModule(configuration);

            //Configure Identity
            services.AddIdentity<User, Role>()
                    .AddEntityFrameworkStores<FreelanceDbContext>()
                    .AddUserStore<FreelanceUserStore>()
                    .AddRoleStore<FreelanceRoleStore>()
                    .AddUserManager<FreelanceUserManager>()
                    .AddSignInManager<FreelanceSignInManager>()
                    .AddRoleManager<FreelanceRoleManager>()
                    .AddDefaultTokenProviders();

            //Configure DI for services
            services.Scan(scan =>
            {
                scan.FromAssemblies(Assembly.GetExecutingAssembly())
                    .AddClasses(classes => classes.AssignableTo<IService>())
                    .AsImplementedInterfaces()
                    .WithTransientLifetime(); //Transient = instance when required
            });

            //Other
            services.AddTransient(typeof(IDbInitializer), typeof(DbInitializer));
          
            return services;
        }
    }
}
