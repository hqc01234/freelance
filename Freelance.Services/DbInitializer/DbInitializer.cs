﻿using Freelance.Core.DbInitializer;
using Freelance.Core.Identity;
using Freelance.Services.Identity;
using System.Linq;

namespace Freelance.Data.DbInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly FreelanceUserManager userManager;
        private readonly FreelanceRoleManager roleManager;

        public DbInitializer(FreelanceUserManager userManager, FreelanceRoleManager roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public void Initialize()
        {
            if (!this.roleManager.Roles.Any(x => x.Name == "Admin" || x.Name == "User"))
            {
                this.roleManager.CreateAsync(new Role { Name = "Admin" }).Wait();
                this.roleManager.CreateAsync(new Role { Name = "User" }).Wait();
            }

            var defaultAdminAccount = this.userManager.FindByNameAsync("admin").Result;
            if (defaultAdminAccount == null)
            {
                var user = new User { UserName = "admin" };
                this.userManager.CreateAsync(user, "!Abc123").Wait();
                this.userManager.AddToRoleAsync(user, "Admin").Wait();
            }
        }
    }
}
