﻿using Freelance.Core.Identity;
using Freelance.Data.DBContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Freelance.Data.Identity
{
    /*
     * Stores and retrieves role information (such as the role name)
     */
    public class FreelanceRoleStore : RoleStore<Role, FreelanceDbContext, int, UserRole, RoleClaim>
    {
        public FreelanceRoleStore(FreelanceDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
