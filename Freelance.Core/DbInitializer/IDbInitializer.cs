﻿namespace Freelance.Core.DbInitializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
