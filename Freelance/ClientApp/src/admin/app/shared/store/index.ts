import { createSelector }   from 'reselect';
import * as fromApp         from '../store/reducers/app.reducer';
import * as fromCtrSideBar  from '../store/reducers/control-side-bar.reducer';

/**
 * We treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
    app: fromApp.State;
    controlSideBar: fromCtrSideBar.State
}

/**
 * All reducers.
 */
export const reducers = {
    app: fromApp.reducer,
    controlSideBar: fromCtrSideBar.reducer
};

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 */

/**
 * App store functions
 */
export const getAppState     = (state: State) => state.app;
export const getAppMessage   = createSelector(getAppState, fromApp.getMessage);

/**
 * Control side bar store functions
 */
export const getCtrSideBarState = (state: State) => state.controlSideBar;
export const getSelectedSkin    = createSelector(getCtrSideBarState, fromCtrSideBar.getSelectedSkin);
