import { ActionReducer }    from '@ngrx/store'
import * as actions         from '../actions/app.action';

export interface State {
    message: string
}

const INIT_STATE: State = {
    message: "Init Message"
}

export function reducer(state = INIT_STATE, action) {
    switch (action.type) {
        case actions.ActionTypes.CHANGE_MESSAGE: {
            return Object.assign({}, state, { message: action.payload } );
        }
        default: return state;
    }
}

export const getMessage = (state: State) => state.message;

