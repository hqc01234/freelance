import { ActionReducer }    from '@ngrx/store';
import { AppSkin }          from '../../enums';
import * as actions         from '../actions/control-side-bar.action';

export interface State {
    selectedSkin: AppSkin
}

let getInitState = (): State => {
    let _selectedSkin = localStorage.getItem("selectedSkin") || AppSkin.Blue;
    return { selectedSkin: parseInt(_selectedSkin.toString()) as AppSkin };     
}

export function reducer(state = getInitState(), action) {
    switch (action.type) {
        case actions.ActionTypes.SELECT_SKIN: {
            return Object.assign({}, state, { selectedSkin: action.payload });
        }
        default: return state;
    }
}

export const getSelectedSkin = (state: State) => state.selectedSkin;
