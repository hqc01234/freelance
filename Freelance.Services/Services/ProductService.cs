﻿using Freelance.Core.Services;
using Freelance.Data.Entities;
using Freelance.Data.Repositories.Interfaces;

namespace Freelance.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> productRepository;

        public ProductService(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }
    }
}
