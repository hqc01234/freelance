﻿using Microsoft.AspNetCore.Identity;

namespace Freelance.Core.Identity
{
    /*
     * Represents an authentication token for a user.
     */
    public class UserToken : IdentityUserToken<int>
    {
    }
}
