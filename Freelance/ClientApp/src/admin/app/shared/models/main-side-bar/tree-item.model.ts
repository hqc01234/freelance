export class TreeItem {
    public title            : string;
    public routerLink       : string;
    public icon             : string;
    public isHidden?    : boolean;
    public childItems?      : TreeItem[];

    constructor(treeItem: Partial<TreeItem>) {
        Object.assign(this, treeItem);
    }

    public get isHaveChildItem() {
        return this.childItems && this.childItems.length > 0;
    }
}