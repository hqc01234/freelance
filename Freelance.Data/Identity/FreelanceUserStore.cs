﻿using Freelance.Core.Identity;
using Freelance.Data.DBContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Freelance.Data.Identity
{
    /*
     * Stores and retrieves user information (such as user name and password hash) 
     */
    public class FreelanceUserStore : UserStore<User, Role, FreelanceDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
    {
        public FreelanceUserStore(FreelanceDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
