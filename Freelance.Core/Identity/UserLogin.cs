﻿using Microsoft.AspNetCore.Identity;

namespace Freelance.Core.Identity
{
    /*
     * Information about the external authentication provider 
     * (like Facebook or a Microsoft account) to use when logging in a user  
     */
    public class UserLogin : IdentityUserLogin<int>
    {
    }
}
