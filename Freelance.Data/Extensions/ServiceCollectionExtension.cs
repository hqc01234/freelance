﻿using Freelance.Data.DBContext;
using Freelance.Data.Repositories.Interfaces;
using Freelance.Data.Repository.Implement;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Freelance.Data.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDataModule(this IServiceCollection services, IConfiguration configuration)
        {
            //Configure DbContext
            services.AddDbContext<FreelanceDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });
            
            //Repository
            //Scoped = instance per request
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,,>));

            return services;
        }
    }
}
