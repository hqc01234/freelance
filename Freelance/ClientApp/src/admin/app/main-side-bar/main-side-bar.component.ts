import { 
  Component, 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy 
}                                   from '@angular/core';
import { 
  Router, NavigationEnd 
}                                   from '@angular/router';
import { Subscription }             from 'rxjs/Subscription';
import { MAIN_SIDE_BAR_TREE_ITEMS } from './main-side-bar.tree-items';
import { TreeItem }                 from '../shared/models';

@Component({
  selector: 'app-main-side-bar',
  templateUrl: './main-side-bar.component.html',
  styleUrls: ['./main-side-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSideBarComponent implements OnDestroy {

  public treeItems = MAIN_SIDE_BAR_TREE_ITEMS;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef
  ) { 
    this.subscribeEvent();
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(sub => { sub.unsubscribe(); });
  }

  /**
   * Check if we should display tree item or not.
   * Dont display hidden item unless it matches current route
   * @param treeItem 
   */
  public isDisplayTreeItem(treeItem: TreeItem) {
    return !treeItem.isHidden || this.router.isActive(treeItem.routerLink, true);
  }

  private subscribeEvent() {
    this.subscriptions.push(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        //call change detect every time navigation end to update tree view
        this.cdr.detectChanges();
      }
    }));
  }
}
