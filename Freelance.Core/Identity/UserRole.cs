﻿using Microsoft.AspNetCore.Identity;

namespace Freelance.Core.Identity
{
    /*
     * Represents an EntityType user belonging to a role. 
     */
    public class UserRole : IdentityUserRole<int>
    {
    }
}
