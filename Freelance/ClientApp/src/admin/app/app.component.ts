import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  public title = 'app';
  public isLoggedIn = true;

  public ngAfterViewInit(): void {
    let body = document.getElementsByTagName("body")[0];
    
    if (!this.isLoggedIn) {
      body.className = `hold-transition login-page`;
    }
  }
}
