import { 
    Injectable, OnDestroy 
}                               from '@angular/core';
import { Store }                from '@ngrx/store';
import { Subscription }         from 'rxjs/Subscription';
import { Sandbox }              from '../shared/sandbox/base.sandbox';
import { 
    AppSkin, 
    LocalStorageItem 
}                               from '../shared/enums';
import * as store               from '../shared/store';
import * as ctrSideBarActions   from '../shared/store/actions/control-side-bar.action';

const SIDE_BAR_CSS = {
    Collapse : "sidebar-collapse",
    Mini     : "sidebar-mini"
};
const APP_SKIN_CSS = {
    Blue     : "skin-blue", 
    Yellow   : "skin-yellow", 
    Purple   : "skin-purple"
};

@Injectable()
export class ControlSideBarSandbox extends Sandbox implements OnDestroy {

    public selectedSkin$ = this.appState$.select(store.getSelectedSkin);
    private subscriptions: Subscription[] = [];

    constructor(protected appState$: Store<store.State>) {
        super(appState$);
        this.subcribeEvent();
    }

    public ngOnDestroy() {
        this.subscriptions.forEach(sub => { sub.unsubscribe(); });
    }

    public selectSkin(appSkin: AppSkin) {
        this.appState$.dispatch(new ctrSideBarActions.SelectSkinAction(appSkin));
    }

    private subcribeEvent(){
        this.subscriptions.push(this.selectedSkin$.subscribe(appSkin => {          
            let body = document.getElementsByTagName("body")[0];
            body.className = body.className.indexOf(SIDE_BAR_CSS.Collapse) != -1 ?
                `${SIDE_BAR_CSS.Mini} ${SIDE_BAR_CSS.Collapse}` :
                `${SIDE_BAR_CSS.Mini}`;
            
            switch (appSkin) {
                case AppSkin.Blue: {
                    body.classList.add(APP_SKIN_CSS.Blue);
                    break;
                }
                case AppSkin.Purple: {
                    body.classList.add(APP_SKIN_CSS.Purple);
                    break;
                }
                case AppSkin.Yellow: {
                    body.classList.add(APP_SKIN_CSS.Yellow);
                    break;
                }
            }

            localStorage.setItem(LocalStorageItem.selectedSkin, appSkin.toString());
        }));
    }
}