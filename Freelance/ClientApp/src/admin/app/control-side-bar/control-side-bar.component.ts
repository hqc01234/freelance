import { Component, OnInit }  from '@angular/core';
import { AppSkin }            from '../shared/enums/app.enum';
import { 
  ControlSideBarSandbox 
}                             from './control-side-bar.sandbox';

@Component({
  selector    : 'app-control-side-bar',
  templateUrl : './control-side-bar.component.html',
  styleUrls   : ['./control-side-bar.component.css'],
  providers   : [ControlSideBarSandbox]
})
export class ControlSideBarComponent implements OnInit {

  public appSkin = AppSkin;

  constructor(
    public ctrSideBarSandbox: ControlSideBarSandbox 
  ) { }

  public ngOnInit() {
  }

  public selectSkin(appSkin: AppSkin) {
    this.ctrSideBarSandbox.selectSkin(appSkin);
  }
}
