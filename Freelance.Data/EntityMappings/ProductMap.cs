﻿using Freelance.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Freelance.Data.Maps
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Product");
            entityTypeBuilder.HasOne(x => x.Category)
                             .WithMany(x => x.Products)
                             .HasForeignKey(x => x.CategoryId);
        }
    }
}
