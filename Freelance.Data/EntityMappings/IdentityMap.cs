﻿using Freelance.Core.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Freelance.Data.Maps
{
    public class IdentityMap : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetUserRoles");
        }
    }

    public class UserTokenMap : IEntityTypeConfiguration<UserToken>
    {
        public void Configure(EntityTypeBuilder<UserToken> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetUserTokens");
        }
    }

    public class UserLoginMap : IEntityTypeConfiguration<UserLogin>
    {
        public void Configure(EntityTypeBuilder<UserLogin> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetUserLogins");
        }
    }

    public class UserClaimMap : IEntityTypeConfiguration<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetUserClaims");
        }
    }

    public class RoleClaimMap : IEntityTypeConfiguration<RoleClaim>
    {
        public void Configure(EntityTypeBuilder<RoleClaim> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetRoleClaims");
        }
    }

    public class RoleMap : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetRoles");
        }
    }

    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("AspNetUsers");
        }
    }
}
