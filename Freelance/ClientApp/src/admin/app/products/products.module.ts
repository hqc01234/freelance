import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import {
  ReactiveFormsModule,
  FormControl
}                                   from "@angular/forms";
import { RouterModule }             from '@angular/router';
import { ProductsRoutingModule }    from './products-routing.module';
import { ProductsComponent }        from './products.component';
import { ProductDetailsComponent }  from './product-details/product-details.component';
import { 
    ProductCategoriesComponent 
}                                   from './product-categories/product-categories.component';
import { ProductListComponent }     from './product-list/product-list.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ProductsRoutingModule
  ],
  declarations: [
    ProductsComponent,
    ProductDetailsComponent,
    ProductCategoriesComponent,
    ProductListComponent
  ],
  providers: [
  ]
})
export class ProductsModule {}
