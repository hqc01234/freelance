import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { ProductsComponent }        from './products.component';
import { ProductDetailsComponent }  from './product-details/product-details.component';
import { ProductCategoriesComponent }  from './product-categories/product-categories.component';
import { ProductListComponent }  from './product-list/product-list.component';

const productsRoutes: Routes = [
    {
        path: '',
        component: ProductsComponent,
        children: [
            {
                path        : '',
                redirectTo  : 'product-list',
                pathMatch   : 'full'
            },
            {
                path        : 'product-list',
                component   : ProductListComponent,
            },
            {
                path        : 'product-details/:id',
                component   : ProductDetailsComponent,
            },
            {
                path        : 'product-categories',
                component   : ProductCategoriesComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(productsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductsRoutingModule { }
