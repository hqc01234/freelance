import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthenticationComponent }  from './authentication.component';
import { LoginComponent }           from './login/login.component';
import { SignOutComponent }         from './sign-out/sign-out.component';

const authenticationRoutes: Routes = [
    {
        path: '',
        component: AuthenticationComponent,
        children: [
            {
                path        : '',
                redirectTo  : 'login',
                pathMatch   : 'full'
            },
            {
                path        : 'login',
                component   : LoginComponent,
            },
            {
                path        : 'sign-out',
                component   : SignOutComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(authenticationRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthenticationRoutingModule { }
