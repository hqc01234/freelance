// Angular core modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Store
import { reducers } from './shared/store';

// Third party libraries
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

//App components
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainSideBarComponent } from './main-side-bar/main-side-bar.component';
import { ControlSideBarComponent } from './control-side-bar/control-side-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent,
    MainSideBarComponent,
    ControlSideBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers)
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
