import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import {
  ReactiveFormsModule,
  FormsModule,
  FormControl
}                                   from '@angular/forms';
import { RouterModule }             from '@angular/router';
import { 
    AuthenticationRoutingModule 
}                                   from './authentication-routing.module';
import { AuthenticationComponent }  from './authentication.component';
import { LoginComponent }           from './login/login.component';
import { RegisterComponent }        from './register/register.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    AuthenticationRoutingModule
  ],
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [
  ]
})
export class AuthenticationModule {}
