﻿using Freelance.Core.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Freelance.Services.Identity
{
    public class FreelanceRoleManager : RoleManager<Role>
    {
        public FreelanceRoleManager(
            IRoleStore<Role> store, 
            IEnumerable<IRoleValidator<Role>> roleValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            ILogger<FreelanceRoleManager> logger) 
            : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }
}
