import { Action }   from '@ngrx/store';
import { type }     from '../../utilities';
import { AppSkin }  from '../../enums';

export const ActionTypes = {
    SELECT_SKIN: type("[CONTROL SIDE BAR] Select skin")
}

export class SelectSkinAction implements Action {
    public type: string = ActionTypes.SELECT_SKIN;
    constructor(public payload: AppSkin) {}
}

export type Actions = SelectSkinAction;
