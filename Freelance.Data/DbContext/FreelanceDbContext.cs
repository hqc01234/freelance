﻿using Freelance.Core.Identity;
using Freelance.Data.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Freelance.Data.DBContext
{
    public class FreelanceDbContext : IdentityDbContext<User, Role, int>
    {
        public FreelanceDbContext(DbContextOptions<FreelanceDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.AddEntityConfigurationsFromAssembly(Assembly.GetExecutingAssembly());                
        }
    }
}
