import { Action }   from '@ngrx/store';
import { type }     from '../../utilities';

export const ActionTypes = {
    CHANGE_MESSAGE: type('[APP] Change message')
}

export class ChangeMessageAction implements Action {
    public type =  ActionTypes.CHANGE_MESSAGE;
    constructor(public payload: string) { }
}

export type Actions = ChangeMessageAction;