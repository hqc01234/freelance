export enum AppSkin {
    Blue    = 1, 
    Yellow  = 2, 
    Purple  = 3
}

export const LocalStorageItem = {
    selectedSkin: "selectedSkin"
}